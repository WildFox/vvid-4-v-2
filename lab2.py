import math

def check_value (value) : # Проверка на коррекность данных
    try:
        result = float(value)
    except ValueError:
        print("Неверный формат числа")
        value = check_value(input("Введите корректно координату: "))
    except Exception:
        print("Незвестная ошибка")
        value = check_value(input("Введите корректно координату: "))
    else:
        return result

def cg_distance(x1, y1, x2, y2, x3, y3, x4, y4): # Растояние между центрами тяжести
    centerx1 = (x1 + x2) / 2
    centery1 = (y1 + y2) / 2
    centerx2 = (x3 + x4) / 2
    centery2 = (y3 + y4) / 2
    distance = math.sqrt((centerx2 - centerx1) ** 2 + (centery2 - centery1) ** 2)
    return round(distance, 2)

def corner_distance (x1, y1, x2, y2, x3, y3, x4, y4):
    distance_1 = math.sqrt((x3 - x1) ** 2 + (y3 - y1) ** 2)
    distance_2 = math.sqrt((x4 - x2) ** 2 + (y4 -y2) ** 2)
    distance_sum = distance_1 + distance_2
    return round(distance_sum, 2)

x1 = None
choice = 0;

while choice == 0:
    print("Меню:")
    print("1. Ввод данных")
    print("2. Найти расстояние между центрами тяжести двух прямоугольников")
    print("3. Найти сумму расстояний между верхними левыми и нижними правыми углами двух прямоугольников")
    print("0. Выход из программы")

    choice = input("Выберите пункт меню: ")

    if choice == "1":
        x1 = check_value(input("Введите координату x1: "))
        y1 = check_value(input("Введите координату y1: "))
        x2 = check_value(input("Введите координату x2: "))
        y2 = check_value(input("Введите координату y2: "))
        x3 = check_value(input("Введите координату x3: "))
        y3 = check_value(input("Введите координату y3: "))
        x4 = check_value(input("Введите координату x4: "))
        y4 = check_value(input("Введите координату y4: "))
        print("Координаты введены успешно ")
        choice = 0;

    elif choice == "2":
        if x1 == None:
            print("Сначала нужно ввести данные")
        else:
            result = cg_distance(x1, y1, x2, y2, x3, y3, x4, y4)
            print("Расстояние между центрами тяжести двух прямоугольников:", result)
        choice = 0;

    elif choice == "3":
        if x1 == None:
            print("Сначала нужно ввести данные")
        else:
            result = corner_distance(x1, y1, x2, y2, x3, y3, x4, y4)
            print("Сумма расстояний между верхними левыми и нижними правыми углами двух прямоугольников:", result)
        choice = 0;

    elif choice != "0":
        print("Некорректный выбор")
        choice = 0;
